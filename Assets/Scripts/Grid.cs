﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class Grid : MonoBehaviour
{
    public static Cell[] CellsAll;
    public static List<Cell> CellsMined;
    public static List<Cell> CellsUnMined;
    public static GameState State = GameState.InGame;
    public static int CellsMarkedCorrectly = 0;
    public static int CellsUncovered = 0;
    public static int CellsRemaining = 0;
    public float DistanceBetweenCells = 1.0f;
    public Cell CellPrefab;
    public int CellsPerRow = 8;
    public int NumberOfMines = 10;
    public int NumberOfCells = 64;

    // Use this for initialization
    private void Start()
    {
        CreateCells();

        CellsRemaining = NumberOfMines;
        CellsMarkedCorrectly = 0;
        CellsUncovered = 0;

        State = GameState.InGame;
    }

    // Update is called once per frame
    private void Update()
    {
        if (State == GameState.InGame)
        {
            if ((CellsUncovered == NumberOfCells - NumberOfMines) ||
                (CellsRemaining == 0 && CellsMarkedCorrectly == NumberOfMines))
            {
                FinishGame();
            }
        }
        if (Input.GetKeyDown("space"))
        {
            Restart();
        }
    }

    private void OnGUI()
    {
        if (State == GameState.InGame)
        {
            GUI.Box(new Rect(10, 10, 200, 50), "Mines left: " + CellsRemaining);
        }
        else if (State == GameState.GameOver)
        {
            GUI.Box(new Rect(10, 10, 200, 50), "GAME OVER");

            if (GUI.Button(new Rect(10, 70, 200, 50), "Restart"))
            {
                Restart();
            }
        }
        else if (State == GameState.GameWon)
        {
            GUI.Box(new Rect(10, 10, 200, 50), "GAME WON");

            if (GUI.Button(new Rect(10, 70, 200, 50), "Restart"))
            {
                Restart();
            }
        }
        var cursor = GameObject.Find("Cursor").GetComponent(typeof (Cursor)) as Cursor;
        GUI.Box(new Rect(10, 150, 200, 50), cursor.State.ToString());
    }

    private void Restart()
    {
        State = GameState.Loading;
        Application.LoadLevel(Application.loadedLevel);
    }

    private void CreateCells()
    {
        float xOffset = 0.0f;
        float yOffset = 0.0f;

        CellsAll = new Cell[NumberOfCells];
        CellsMined = new List<Cell>();
        CellsUnMined = new List<Cell>();

        for (int cellsCreated = 0; cellsCreated < NumberOfCells; cellsCreated += 1)
        {
            xOffset += DistanceBetweenCells;

            if (cellsCreated%CellsPerRow == 0)
            {
                yOffset += DistanceBetweenCells;
                xOffset = 0;
            }

            var cell = Instantiate(CellPrefab,
                new Vector3(transform.position.x + xOffset, transform.position.y + yOffset, transform.position.z + 3),
                Quaternion.Euler(270, 0, 0)) as Cell;
            cell.Id = cellsCreated;
            cell.CellsPerRow = CellsPerRow;

            CellsAll[cellsCreated] = cell;
        }
        print(CellsAll.Length);

        AssignMines();
    }

    private void AssignMines()
    {
        CellsUnMined = CellsAll.ToList();

        for (int minesAssigned = 0; minesAssigned < NumberOfMines; minesAssigned += 1)
        {
            Cell cell = CellsUnMined.ElementAt(Random.Range(0, CellsUnMined.Count));

            CellsMined.Add(cell);
            CellsUnMined.Remove(cell);

            cell.GetComponent<Cell>().IsMined = true;
        }
    }

    private void FinishGame()
    {
        State = GameState.GameWon;

        // uncovers remaining field if all nodes have been placed
        foreach (Cell cell in CellsAll)
        {
            if (cell.State == CellState.Idle && !cell.IsMined)
            {
                cell.UncoverCellAdvanced();
            }
        }

        // marks remaining cells if all nodes except the cells have been uncovered
        foreach (Cell cell in CellsMined)
        {
            if (cell.State != CellState.Flagged)
            {
                cell.SetFlag();
            }
        }
    }
}