﻿public enum CellState
{
    Idle,
    Flagged,
    Uncovered,
    Detonated
}