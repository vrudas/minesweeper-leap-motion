﻿using System;
using System.Collections.Generic;
using UnityEngine;

public class Cell : MonoBehaviour
{
    public List<Cell> AdjacentCells = new List<Cell>();
    public int AdjacentMinesCount = 0;
    public TextMesh DisplayText;
    public GameObject Flag;
    public int Id;
    public bool IsMined = false;
    public Material MaterialDetonated;
    public Material MaterialIdle;
    public Material MaterialLightUp;
    public Material MaterialUncovered;
    public Cell CellLeft;
    public Cell CellLower;
    public Cell CellLowerLeft;
    public Cell CellLowerRight;
    public Cell CellRight;
    public Cell CellUpper;

    public Cell CellUpperLeft;
    public Cell CellUpperRight;
    public int CellsPerRow;
    public CellState State = CellState.Idle;

    // Use this for initialization
    private void Start()
    {
        DisplayText.renderer.enabled = false;
        Flag.renderer.enabled = false;

        GenerateNeighboringMines();
        CountMines();
    }

    // Update is called once per frame
    private void Update()
    {
    }

    private void OnTriggerEnter(Collider collider)
    {
        if (collider.gameObject.name == "Cursor")
        {
            var cursor = GameObject.Find("Cursor").GetComponent(typeof (Cursor)) as Cursor;
            if (Grid.State == GameState.GameOver || Grid.State == GameState.GameWon) return;
            if (cursor.State == CursorState.Cell)
            {
                if (State != CellState.Flagged)
                {
                    UncoverCell();                    
                }
            }
            else
            {
                SetFlag();
            }
        }
    }

    private void OnMouseOver()
    {
        if (Grid.State != GameState.InGame) return;
        switch (State)
        {
            case CellState.Idle:
                renderer.material = MaterialLightUp;
                if (Input.GetMouseButtonDown(0))
                {
                    UncoverCell();
                }

                if (Input.GetMouseButtonDown(1))
                {
                    SetFlag();
                }
                break;
            case CellState.Flagged:
                renderer.material = MaterialLightUp;
                if (Input.GetMouseButtonDown(1))
                {
                    SetFlag();
                }
                break;
        }
    }

    private void OnMouseExit()
    {
        if (Grid.State != GameState.InGame) return;
        if (State == CellState.Idle || State == CellState.Flagged)
        {
            renderer.material = MaterialIdle;
        }
    }

    public void CountMines()
    {
        AdjacentMinesCount = 0;

        foreach (Cell currentMine in AdjacentCells)
        {
            if (currentMine.IsMined)
            {
                AdjacentMinesCount++;
            }
        }

        DisplayText.text = AdjacentMinesCount.ToString();

        if (AdjacentMinesCount <= 0)
        {
            DisplayText.text = "";
        }
    }

    private void GenerateNeighboringMines()
    {
        if (InBounds(Grid.CellsAll, Id + CellsPerRow))
        {
            CellUpper = Grid.CellsAll[Id + CellsPerRow];
            AdjacentCells.Add(CellUpper);
        }
        if (InBounds(Grid.CellsAll, Id - CellsPerRow))
        {
            CellLower = Grid.CellsAll[Id - CellsPerRow];
            AdjacentCells.Add(CellLower);
        }
        if (InBounds(Grid.CellsAll, Id - 1) && Id%CellsPerRow != 0)
        {
            CellLeft = Grid.CellsAll[Id - 1];
            AdjacentCells.Add(CellLeft);
        }
        if (InBounds(Grid.CellsAll, Id + 1) && (Id + 1)%CellsPerRow != 0)
        {
            CellRight = Grid.CellsAll[Id + 1];
            AdjacentCells.Add(CellRight);
        }

        if (InBounds(Grid.CellsAll, Id + CellsPerRow + 1) && (Id + 1)%CellsPerRow != 0)
        {
            CellUpperRight = Grid.CellsAll[Id + CellsPerRow + 1];
            AdjacentCells.Add(CellUpperRight);
        }
        if (InBounds(Grid.CellsAll, Id + CellsPerRow - 1) && Id%CellsPerRow != 0)
        {
            CellUpperLeft = Grid.CellsAll[Id + CellsPerRow - 1];
            AdjacentCells.Add(CellUpperLeft);
        }
        if (InBounds(Grid.CellsAll, Id - CellsPerRow + 1) && (Id + 1)%CellsPerRow != 0)
        {
            CellLowerRight = Grid.CellsAll[Id - CellsPerRow + 1];
            AdjacentCells.Add(CellLowerRight);
        }
        if (InBounds(Grid.CellsAll, Id - CellsPerRow - 1) && Id%CellsPerRow != 0)
        {
            CellLowerLeft = Grid.CellsAll[Id - CellsPerRow - 1];
            AdjacentCells.Add(CellLowerLeft);
        }
    }

    private bool InBounds(Cell[] cells, int targetMineId)
    {
        return targetMineId >= 0 && targetMineId < cells.Length;
    }

    public void SetFlag()
    {
        switch (State)
        {
            case CellState.Idle:
                State = CellState.Flagged;
                Flag.renderer.enabled = true;
                Grid.CellsRemaining--;
                if (IsMined)
                {
                    Grid.CellsMarkedCorrectly++;
                }
                break;
            case CellState.Flagged:
                State = CellState.Idle;
                Flag.renderer.enabled = false;
                Grid.CellsRemaining++;
                if (IsMined)
                {
                    Grid.CellsMarkedCorrectly--;
                }
                break;
        }
    }

    private void UncoverCell()
    {
        if (!IsMined)
        {
            State = CellState.Uncovered;
            DisplayText.renderer.enabled = true;
            renderer.material = MaterialUncovered;

            Grid.CellsUncovered++;

            if (AdjacentMinesCount == 0)
            {
                UncoverAdjacentCells();
            }
        }
        else
        {
            Explode();
        }
    }

    private void Explode()
    {
        State = CellState.Detonated;
        renderer.material = MaterialDetonated;

        foreach (Cell currentMine in Grid.CellsMined)
        {
            currentMine.ExplodeAdvanced();
        }

        Grid.State = GameState.GameOver;
    }

    private void ExplodeAdvanced()
    {
        State = CellState.Detonated;
        renderer.material = MaterialDetonated;
    }

    private void UncoverAdjacentCells()
    {
        foreach (Cell cell in AdjacentCells)
        {
            //uncover all adjacent nodes with 0 adjcent cells
            if (!cell.IsMined && cell.State == CellState.Idle && cell.AdjacentMinesCount == 0)
            {
                cell.UncoverCell();
            }
                // uncover all adjacent nodes with more than 1 adjacent mine, then stop uncovering
            else if (!cell.IsMined && cell.State == CellState.Idle && cell.AdjacentMinesCount > 0)
            {
                cell.UncoverCellAdvanced();
            }
        }
    }

    public void UncoverCellAdvanced()
    {
        State = CellState.Uncovered;
        DisplayText.renderer.enabled = true;
        renderer.material = MaterialUncovered;
        Grid.CellsUncovered++;
    }
}