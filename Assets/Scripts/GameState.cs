﻿public enum GameState
{
    InGame,
    Loading,
    GameOver,
    GameWon
}