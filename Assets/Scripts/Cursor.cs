﻿using System;
using Leap;
using UnityEngine;

public class Cursor : MonoBehaviour
{
    public CursorState State = CursorState.Cell;
    private LeapManager _leapManager;
    private Camera _mainCam;
    public float _projectionDistance = 5;
    public Material MinesMaterial;
    public Material FlagsMaterial;
    // Use this for initialization
    private void Start()
    {
        InitController();
    }

    private void InitController()
    {
        _mainCam = GameObject.FindGameObjectWithTag("MainCamera").GetComponent(typeof (Camera)) as Camera;
        _leapManager = GameObject.Find("LeapManager").GetComponent(typeof (LeapManager)) as LeapManager;
        if (_leapManager != null)
        {
            _leapManager.leapController.EnableGesture(Gesture.GestureType.TYPECIRCLE);
            _leapManager.leapController.Config.SetFloat("Gesture.Circle.MinRadius", 10.0f);
            _leapManager.leapController.Config.SetFloat("Gesture.Circle.MinArc", .5f);
            _leapManager.leapController.Config.Save();
            renderer.material = MinesMaterial;
        }
    }

    // Update is called once per frame
    private void Update()
    {
        CursorControlLogic();
    }

    private void CursorControlLogic()
    {
        if (_leapManager == null) return;
        if (_leapManager.pointerAvailible)
        {
            transform.position = _leapManager.pointerPositionWorld;
        }

        Gesture gesture = _leapManager.currentFrame.Gestures()[0];
        var circleGesture = new CircleGesture(gesture);
        if (circleGesture.Pointable.Direction.AngleTo(circleGesture.Normal) <= Math.PI/2)
        {
            if (circleGesture.Progress > 1)
            {
                State = CursorState.Flags;
                renderer.material = FlagsMaterial;
            }
        }
        else
        {
            if (circleGesture.Progress > 1)
            {
                State = CursorState.Cell;
                renderer.material = MinesMaterial;
                if (Grid.State == GameState.GameWon || Grid.State == GameState.GameOver)
                {
                    Application.LoadLevel(Application.loadedLevel);
                }
            }
        }
    }
}